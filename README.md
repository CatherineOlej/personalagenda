# Personal Agenda

This project is licensed under the MIT LICENSE. This means that you have permission to use the code, modify it, publish it, or sell it. 
Restrictions that apply to this LICENSE is are crediting the author of this application, so you cannot remove the license and copyright text from
the code. I am sharing my work as there is always room for improvement. All are welcome. 

This project is a simple personal agenda that allows users to create a task that can be set in the present, the next day or future. This web application operates with CRUD functions and webSQL as a database to store user inputs.

The development process of this project was done in visual studio code. 
The programming languages used for this devlopment are:
•	 JavaScript
•	 jQuery
•	 HTML5/CSS3

To get started on using this web application, you can simply download the repository and access it's full features
from the web browser HTML document (this is found in the project folder).

The application is straight forward to use. Before you begin you must replace the code in the header first, so the application can function properly. The code you need to replace is provided for you. 
It is text file labeled as "replaceThisCodeInHeadOfIndex"; simply copy and paste it in the head section of the index file.
As you run the project you will land on the home page where you will
see real time displayed at the top and three options for creating a task. However, once you click on "Create (Today's, Tomorrow's, or Future) Task" you will be taken to the tasks page where you will
be able to create your first task. Make sure you specify the task priority, although it is optional. Fill in the fields and click on the submit button. Once you have successfully created a task an alert box
will pop up to inform you of so. After you have created a task you can see it under the 'TODO' tab (a.k.a #ViewTasksPage). If for any reason you wish to make a change to the task you can click on it and you will
have the option to either update or delete the task. The settings page is where you can clear the Database. 

